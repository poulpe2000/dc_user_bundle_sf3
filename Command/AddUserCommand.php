<?php
namespace DC\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DC\UserBundle\Entity\User;

class AddUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dc:user:add')
            ->setDescription('Ajouter un utilisateur')
            ->addArgument('username', InputArgument::REQUIRED, 'Le login')
            ->addArgument('password', InputArgument::REQUIRED, 'Le mot de passe')
            ->addArgument('email', InputArgument::REQUIRED, 'email');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $email = $input->getArgument('email');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
// encode the password
//$factory = $this->getContainer()->get('security.encoder_factory');
//$encoder = $factory->getEncoder($user);
//$encodedPassword = $encoder->encodePassword($password, $user->getSalt());
//$user->setPassword($encodedPassword);

        $encoder = $this->getContainer()->get('security.password_encoder');
        $password = $encoder->encodePassword($user, $password);


//        $user->setPassword($this->getContainer()->get("security.encoder_factory")->getEncoder($user)->encodePassword($password, $user->getSalt()));
        $user->setPassword($password);
        $em->persist($user);
        $em->flush();

        $output->writeln(sprintf('Le user %s avec le mot de passe %s a été ajouté', $username, $password));
    }
}