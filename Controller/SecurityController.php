<?php

namespace DC\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SecurityController extends Controller
{
    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');
        return $this->render('DCUserBundle:Security:login.html.twig', array(
            'last_username' => $helper->getLastUsername(),
            'error'         => $helper->getLastAuthenticationError(),
        ));
    }

}
